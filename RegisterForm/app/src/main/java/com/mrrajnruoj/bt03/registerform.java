package com.mrrajnruoj.bt03;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.ArrayList;

public class registerform extends Activity {
    EditText edtUsername, edtPassword, edtRetype, edtBirthday;
    RadioButton rdMale, rdFemale;
    RadioGroup rdGrGender;
    CheckBox ckbTennis, ckbFutbal, ckbOthers;
    Button btnSelect, btnReset, btnSignup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registerform);

        mapComponent();
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearForm();
            }
        });
        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtPassword.getText().toString().equals(edtRetype.getText().toString()) == true) {
                    Intent intent = new Intent(registerform.this, resultform.class);

                    Bundle bundle = new Bundle();
                    bundle.putString("username", edtUsername.getText().toString());
                    bundle.putString("password", edtPassword.getText().toString());
                    bundle.putString("birthday", edtBirthday.getText().toString());
                    bundle.putString("gender", rdMale.isChecked() == true ? "Male" : "Female");
                    ArrayList<String> hobbies = new ArrayList<>();
                    if (ckbTennis.isChecked()) hobbies.add(ckbTennis.getText().toString());
                    if (ckbFutbal.isChecked()) hobbies.add(ckbFutbal.getText().toString());
                    if (ckbOthers.isChecked()) hobbies.add(ckbOthers.getText().toString());
                    bundle.putStringArrayList("hobbies", hobbies);
                    intent.putExtras(bundle);
                    startActivityForResult(intent, 12345);
                } else {
                    Toast.makeText(registerform.this, "Mật khẩu không khớp. Mời nhập lại!", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    void mapComponent() {
        edtUsername = (EditText)findViewById(R.id.edtUsername);
        edtPassword = (EditText)findViewById(R.id.edtPassword);
        edtRetype = (EditText)findViewById(R.id.edtRetypePassword);
        edtBirthday = (EditText)findViewById(R.id.edtBirthday);
        rdMale = (RadioButton)findViewById(R.id.rdMale);
        rdFemale = (RadioButton)findViewById(R.id.rdFemale);
        ckbTennis = (CheckBox)findViewById(R.id.ckbTennis);
        ckbFutbal = (CheckBox)findViewById(R.id.ckbFutbal);
        ckbOthers = (CheckBox)findViewById(R.id.ckbOthers);
        btnReset = (Button)findViewById(R.id.btnReset);
        btnSelect = (Button)findViewById(R.id.btnSelect);
        btnSignup = (Button)findViewById(R.id.btnSignup);
        rdGrGender = (RadioGroup)findViewById(R.id.rdGroupGender);
    }

    void clearForm() {
        edtUsername.setText("");
        edtPassword.setText("");
        edtRetype.setText("");
        edtBirthday.setText("");
        rdGrGender.clearCheck();
        ckbTennis.setChecked(false);
        ckbFutbal.setChecked(false);
        ckbOthers.setChecked(false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == 12345 && resultCode == Activity.RESULT_OK) {
                Bundle bundle = data.getExtras();
                Boolean isExit = bundle.getBoolean("Exit");
                if (isExit) finish();
            }
        } catch (Exception e) {
        }
    }
}
