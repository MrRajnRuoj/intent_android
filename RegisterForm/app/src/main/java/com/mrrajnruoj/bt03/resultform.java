package com.mrrajnruoj.bt03;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

public class resultform extends Activity {
    TextView txtUsername, txtPassword, txtBirthday, txtGender, txtHobbies;
    EditText edtPassword, edtBirthday;
    Button btnExit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultform);

        mapComponent();

        final Intent intent = getIntent();
        final Bundle bundle = intent.getExtras();
        txtUsername.setText("Username: " + bundle.getString("username"));
        String strPassword = "";

        for (int i = 0; i < bundle.getString("password").length(); ++i) strPassword += "*";
        txtPassword.setText("Password: " + strPassword);
        txtBirthday.setText("Birthday: " + bundle.getString("birthday"));
        txtGender.setText("Gender: " + bundle.getString("gender"));
        ArrayList<String> hobbies = bundle.getStringArrayList("hobbies");

        String strHobbies = "Hobbies: ";
        for (int i = 0; i < hobbies.size() - 1; ++i) {
            strHobbies += (hobbies.get(i) + ", ");
        }
        strHobbies += hobbies.get(hobbies.size() - 1);
        txtHobbies.setText(strHobbies);
        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle.putBoolean("Exit", true);
                intent.putExtras(bundle);
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });
    }

    void mapComponent() {
        txtUsername = (TextView)findViewById(R.id.txtUsername);
        txtPassword = (TextView)findViewById(R.id.txtPassword);
        txtBirthday = (TextView)findViewById(R.id.txtBirthday);
        txtGender = (TextView)findViewById(R.id.txtGender);
        txtHobbies = (TextView)findViewById(R.id.txtHobbies);
        edtPassword = (EditText)findViewById(R.id.edtPassword);
        edtBirthday = (EditText)findViewById(R.id.edtBirthday);
        btnExit = (Button)findViewById(R.id.btnExit);
    }
}
